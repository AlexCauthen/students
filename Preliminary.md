## Initial assignment (Due Aug 22)

1. BitBucket
   * Sign up for [Atlassian](https://bitbucket.org/account/signup/) if not already signed
     up. Pick "Bitbucket only".
   * [Create ssh key](https://help.github.com/articles/generating-an-ssh-key/)
      - Do steps 1, 2, 4, and 5
      - Do Not Share Your Private Key in id_rsa
	  - For more detail see relevant parts of the instructions for Second Assignment below
   * [Fork](https://confluence.atlassian.com/bitbucket/fork-a-teammate-s-repository-774243391.html) [students repository](https://bitbucket.org/fdac/students) add your interests and create a [pull request](https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html) so that I can add you to the to the BitBucket team for the course.

		1. Start by [**forking** the students repository](https://bitbucket.org/fdac/students): pick action Fork from the little "..." icon near the top of left frame
		1. Add your BitBucket username as USERNAME.md (click on '+' - add 
         new file next to the https//bitbucket.org/USERNAME/students/+ link)
		1. Add your UTK netid and publickey key (found in id_rsa.pub) to USERNAME.pub
		1. Click on Create Pull Request
1. Familiarize yourself with BitBucket workflow
   * Walk through [workflow](#workflow) 
    
## Workflow
1. To start, [**fork** the repository][forking] for the exercise/project
1. [**Clone**][ref-clone] the repository to your computer.
1. View, create, and edit your ipython notebooks or other files
1. [**commit**][ref-commit] changes to complete your solution.
1. [**Push**][ref-push]/sync the changes up to BitBucket.
1. [Create a **pull request**][pull-request] on the original
   repository by the due time (generally within a week)
1. You can continue to push fixes and improvements until the close
date – just add a comment in the pull request to let me know it's been updated.

Feedback will be given in the pull request, so please respond with
your thoughts and questions!  You are welcome to open the pull
request as the work is still in-progress if you are stuck and want
to ask a question – just mention `@audris` with the question to make
sure I know to look at it sooner


## Second Assignment -- configuring ssh: Due Aug 26 

Important note: you need to complete the first assignment so I can add you to the team and 
to the list of server users before you can do the second assignment. 

  * On linux/mac either 
     * create .ssh/config
    	1. create ~/.ssh/config
        ```
         host da2
            hosthostname da2.eecs.utk.edu
            port YOURPORT from students/ports.md
            username YOURNETID
         ```

        1. place your private key in ~/.ssh/id_rsa
        1. Make sure permissions are right
         ```
          chmod -R og-rwx ~/.ssh
         ```
        1. ssh da2
    * Or ssh directly 
      ```
       ssh -pYOURPORT -L8888:localhost:8888 -i id_rsa yournetid@da2.eecs.utk.edu
      ```

  * Putty is a common ssh client for windows
  * [Instructions on how to generate ssh key running windows](https://docs.joyent.com/public-cloud/getting-started/ssh-keys/generating-an-ssh-key-manually/manually-generating-your-ssh-key-in-windows)
    1. ![public ssh key from puttygen](https://bitbucket.org/fdac/news/raw/master/puttykey.png "public ssh key from puttygen")
    1. Save the private key and use it in your putty ssh session
    1. Copy the public key (highlited in the image) to add to the list.txt 
    1. Now work on creating and saving session: start putty and go to connection/ssh/tunnels, enter source and destination and click *add*
    1. ![port forwarding](https://bitbucket.org/fdac/news/raw/master/puttyport.png "select port forwarding")

    1. Go to  go to connection/ssh/Auth and browse for your private key
    1. ![authentication](https://bitbucket.org/fdac/news/raw/master/puttyauth.png "select secret key that was saved above")
    1. Go to  go to session enter hostname and *YOUR PORT* from ports.md in fdac/students
    1. ![session](https://bitbucket.org/fdac/news/raw/master/puttysession.png "start putty session")
    1. Don't forget to _save_ the session before clicking open  



## References

[forking]: https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html
[ref-clone]: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html
[ref-commit]: https://confluence.atlassian.com/bitbucket/push-updates-to-a-repo-221449525.html
[ref-push]: https://confluence.atlassian.com/bitbucket/push-updates-to-a-repo-221449525.html
[pull-request]: https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html
